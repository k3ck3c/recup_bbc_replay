#!/usr/bin/env python                                                                                                                                                   
# -*- coding:utf-8 -*-                                                                                                                                                  
# bbc_proxy version 0.2 par k3c, sortie quand deja telecharge                                                                                                                                         
# passage de proxy en 2 ème parametre                                                                                                                                   
import subprocess, sys, shlex
import random
import urllib2
import requests
import bs4 as BeautifulSoup
import socket
from threading   import Thread

ON_POSIX = 'posix' in sys.builtin_module_names
Finished = False

listeUserAgents = [ 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_5; fr-fr) AppleWebKit/525.18 (KHTML, like Gecko) Version/3.1.2 Safari/525.20.1',
                                                'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1',
                                                'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13\
',
                                                'Mozilla/5.0 (X11; U; Linux x86_64; en-us) AppleWebKit/528.5+ (KHTML, like Gecko, Safari/528.5+) midori',
                                                'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.107 Safari/535.1',
                                                'Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us) AppleWebKit/312.1 (KHTML, like Gecko) Safari/312',
                                                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11',
                                                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.8 (KHTML, like Gecko) Chrome/17.0.940.0 Safari/535.8' ]
allTimeouts = (3, 10, 15, 20)

class FlushFile(object):
    """Write-only flushing wrapper for file-type objects."""
    def __init__(self, f):
        self.f = f
    def write(self, x):
        self.f.write(x)
        self.f.flush()

def start_thread(func, *args):
    t = Thread(target=func, args=args)
    t.daemon = True
    t.start()
    return t

def consume(infile):
    global Finished
    for line in iter(infile.readline, ''):
        if 'Finished writing to temp file.' in line:
            Finished = True
        if 'Already in history' in line:
            Finished = True
        print line,

    infile.close()

def getProxy():
#    opener.addheaders = [('User-agent', random.choice(listeUserAgents))]
    r = requests.get('http://free-proxy-list.net/uk-proxy.html')
    
    soup = BeautifulSoup.BeautifulSoup(r.text)
    lst = []
    for tr in soup.tbody.findAll('tr')[1:-1]:
        i = 0
        slst = []
        for td in tr.find_all('td'):
            i += 1
            if i in (1, 2, 5):
                slst.append(td.contents[0])
            elif i == 8:
                i = 0
                lst.append(slst)
                slst = []
    f = open('foo.lis','w')
    print >>f, lst
    f.close()
    for href in lst:
        yield href

def getValidProxy():
    for timeout in allTimeouts:
        print 'Timeout =', timeout
        socket.setdefaulttimeout(timeout)
        for host, port, typeproxy in getProxy():
            try:
                print 'Trying %s:%s' % (host, port)
                params = (host, int(port))
#                buffer_size = 1024
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(params)
                s.close()
                yield host, port, typeproxy
                #   except urllib2.URLError:
                #       pass
            except socket.timeout:
                pass
            except socket.error:
                pass

def main():
    global Finished
    # Replace stdout with an automatically flushing version
    sys.stdout = FlushFile(sys.__stdout__)
    sys.stderr = FlushFile(sys.__stderr__)
    ftrace = open('/home/gg/bbc1/ftrace', 'w')
    print >>ftrace, "foo"
    print >>ftrace, len(sys.argv)
    idvideo = sys.argv[1]
    print >>ftrace, idvideo
    for host, port, typeproxy in getValidProxy():
        print host, port, typeproxy
        if len(sys.argv) > 1 and sys.argv[2] == '1':

            cmds = "get_iplayer --get "+idvideo + " --nopurge --modes=best --subtitles --force -p 'http://"+host+":"+port+"'"
        else:
            cmds = "get_iplayer --get "+idvideo + " --nopurge --modes=best --subtitles -p 'http://"+host+":"+port+"'"

        arguments = shlex.split(cmds)
        print >>ftrace, "cmds", host, port, cmds
        Finished = False
        process = subprocess.Popen(arguments, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        pid = process.pid
        print >>ftrace, 'le pid de get_iplayer est ', pid
        thread1 = start_thread(consume, process.stdout)
        thread2 = start_thread(consume, process.stderr)
        thread1.join() # wait for IO completion
        thread2.join() # wait for IO completion
        retcode = process.wait()
        print retcode
        if Finished:
            break

if __name__ == "__main__":
    main()

