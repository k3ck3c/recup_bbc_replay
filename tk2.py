#!/usr/bin/python
#-*- coding:utf-8 -*-
#

import os
import os.path
from os import stat
import posixpath
import shutil
import re
import StringIO
import ConfigParser
import subprocess
import glob
from threading   import Thread
try:
    from tkinter import *
except:
    from Tkinter import *
from ScrolledText import ScrolledText
import tkMessageBox
from tkColorChooser import askcolor              
import tkFileDialog
import distutils.dir_util
from Queue import Queue, Empty

ON_POSIX = 'posix' in sys.builtin_module_names
Finished = False
thread1 = None
force = None
root = None
process = None
queue = Queue()
manager = None
txt = None
df = None
snom = None
filesSize = None

def busy():
    global manager, df
    txt.config(cursor="watch")
    df.config(cursor="watch")
    root.config(cursor="watch")

def notbusy():
    global manager, df
    txt.config(cursor="")
    df.config(cursor="")
    root.config(cursor="")

def start_thread(func, *args):
    t = Thread(target=func, args=args)
    t.daemon = True
    t.start()
    return t

def consume(infile):
    for line in iter(infile.readline, ''):
        queue.put(line)
    infile.close()

def callback0():
    global dirfilms, lsb, thread1, txt, nom
    # print 'callback1', dirfilms.get()
    lsb.delete(0, END)
    txt.delete(1.0, END)
    args = ["get_iplayer", dirfilms.get()]
    if refresh.get():
        args.append('-f')
    app = subprocess.Popen(args=args, stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    (stdout, stderr) = app.communicate()
    notbusy()
    if app.returncode == 1:   # sortie en erreur →
        txt.insert(END, 'get_iplayer fail\n')
    else:
        lines = stdout.split('\n')
        for line in lines:
            txt.insert(END, line + '\n')
        n = 1
        for line in lines:
            if line.startswith('Matches:'):
                break
            n += 1
        
        lines = lines[n:-3] if n <= len(lines) else []
        n = 0
        for line in lines:
            line = line.replace('\t', ' ')
            line = line.replace('Added: ','')
            n += 1
            lsb.insert(END, line)
            txt.insert(END, 'get_iplayed success, %s founds\n' % (n,))

def callback1():
    global root
    busy()
    root.after_idle(callback0)

def callback3():
    global lsb, process
    sel = lsb.curselection()
    sel = None if len(sel) == 0 else sel[0]
    print 'callback3', sel
    if sel is not None:
        # print lsb.get(sel)
        # print 'force=', force.get()
        print sel
        print lsb.get(sel)
        nom = lsb.get(sel).split(':')[1].split(',')[0].strip().replace(' ', '_').replace('/', '_').replace("'",'')
        print nom
        global snom,x
        snom ='./'+nom+'*'
        args = ["python", "/home/gg/bbc33.py", lsb.get(sel).split(':')[0], str(force.get())]
        process = subprocess.Popen(args=args, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE, close_fds=ON_POSIX)
        thread1 = start_thread(consume, process.stdout)
        
        # thread1.join() # wait for IO completion
        # if app.returncode == 1:   # sortie en erreur →
        #     print 'ko'
        # else:
        #     print 'ok', stdout
    else:
        print

def on_delete():
    global root, process
    if process:
        process.kill()
    print "sortie du programme"
    root.destroy()

def on_after():
    global queue, txt, root
    while True:
        try:
            v = queue.get_nowait()
        except Empty:
            break;
        txt.insert(END, v)
    root.after(200, on_after)

def on_after2():
    global snom, filesSize
    print snom
    if snom is not None:
        sz = 0
        for fn in glob.glob(snom):
            sz += os.stat(fn).st_size
        filesSize.set('{:,}'.format(sz))
    root.after(10000, on_after2)
    

root = Tk(root)
fr = Frame()
bt1 = Button(fr, text='Films/Séries', command=callback1)
dirfilms = StringVar()
refresh = IntVar()
refresh.set(0)
df = Entry(fr, textvariable=dirfilms, width=100)
df.pack(side=LEFT)
bt1.pack(side=LEFT)
Checkbutton(fr, text="refresh du cache de la BBC", variable=refresh).pack(side=LEFT)
fr.pack()
fr = Frame(root)
scrollbar = Scrollbar(fr)
scrollbar.pack(side=RIGHT, fill=Y)
lsb = Listbox(fr)
lsb.pack(fill=X)
fr.pack(fill=X)
# attach listbox to scrollbar
lsb.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=lsb.yview)
fr = Frame(root)
bt2 = Button(fr, text='Download', command=callback3)
bt2.pack(side=LEFT)
force = IntVar()
force.set(0)
Checkbutton(fr, text="Force", variable=force).pack(side=LEFT)
fr.pack()
fr1 = Frame(fr)
filesSize = StringVar()
filesSize.set('')
Label(fr1, textvariable=filesSize,text=' *********************Taille : **********************',anchor=W, justify=LEFT).pack(side=RIGHT)
fr1.pack(fill=X)

fr = Frame(root)
txt = ScrolledText(fr, width=100,height=50)
txt.pack(fill=X)
fr.pack(fill=X)

root.wm_protocol ("WM_DELETE_WINDOW", on_delete)
root.after(1000, on_after)
root.after(10000, on_after2)

root.mainloop()
